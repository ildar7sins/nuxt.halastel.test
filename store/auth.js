import Cookie from 'cookie';
import Cookies from 'js-cookie';

export const state = () => ({
	token: null
});

export const mutations = {
	setToken(state, token) {
		state.token = token;
	},
	clearToken(state) {
		state.token = null;
	}
};

export const actions = {
	async login({ commit, dispatch }, formData) {
		try {
			const { access_token: token } = await this.$axios.$post('/api/auth/login', formData);
			dispatch('setToken', token);
		} catch (e) {
		  	this.$toast.error(e.response.data.message);
			commit('setError', e, { root: true });
			throw e;
		}
	},
	autoLogin({ dispatch }) {
		const cookiStr = process.browser
		  ? document.cookie
		  : this.app.context.req.headers.cookie;
		const cookies = Cookie.parse(cookiStr || '') || {};
		const token = cookies['token'];
	 
		if (token) {
		  dispatch('setToken', token);
		} else {
		  console.log('Autologin not excepteble', token);
		  dispatch('logout');
		}
	},
	logout({ commit }) {
		this.$axios.setToken(false);
		commit('clearToken');
	    Cookies.remove('token');
	},
	setToken({ commit }, token) {
		this.$axios.setToken(token, 'Bearer');
		commit('setToken', token);
	  	Cookies.set('token', token);
	}
};

export const getters = {
	isAuthenticated: state => Boolean(state.token),
	token: state => state.token
};
