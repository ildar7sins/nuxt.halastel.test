export const state = () => ({
  error: null,
  pageLoader: false,
});

export const mutations = {
  tooglePageLoader(state, error) {
	state.pageLoader = !state.pageLoader;
  },
  setError(state, error) {
	state.error = error;
  },
  clearError(state) {
	state.error = null;
  }
};

export const actions = {
  nuxtServerInit({ dispatch }) {
	dispatch("auth/autoLogin");
  }
};

export const getters = {
  error: state => state.error
};
