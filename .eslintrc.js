module.exports = {
	root: true,
	env: {
		browser: true,
		node: true
	},
	parserOptions: {
		ecmaVersion: 2018,
		sourceType: 'module',
		parser: 'babel-eslint'
	},
	extends: [
	],
	plugins: [
	  'vue'
	],
	// add your custom rules here
	rules: {
		'keyword-spacing': 'off',
		'no-extra-boolean-cast': 'off',
		'max-len': ['error', { code: 120 }],
		'no-trailing-spaces': 0,
		'comma-dangle': 0,
		'operator-linebreak': 0,
		'no-unused-vars': 0,
		'no-undef': 0,
		indent: ['error', 'tab', { SwitchCase: 1 }],
		quotes: [1, 'single'],
		'react/no-unescaped-entities': 0,
		'react/prop-types': 'off',
		'react-native/no-raw-text': 0,
		'space-before-function-paren': 0
	}
};
